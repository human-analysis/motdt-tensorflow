from libs.models.reid.image_part_aligned import Model
import h5py
import numpy as np
import tensorflow as tf

ckpt = 'storage/nets/googlenet_part8_all_xavier_ckpt_56.h5'
h5f = h5py.File(ckpt)
# for key in h5f.keys():
    # print(key, h5f[key][()].shape)

img = tf.placeholder(tf.float32, [4, 64, 64, 3])
model = Model(img)
sess = tf.Session()
assert len(h5f.keys()) == len(tf.trainable_variables())
# for var in tf.trainable_variables():
#     print(var)
# for key in h5f.keys():
#     print(key, h5f[key][()].shape)
for var in tf.trainable_variables():
    key = var.name.replace('/', '.').replace('weights:0', 'weight').replace('biases:0', 'bias')
    value = h5f[key][()]
    if 'weight' in key:
        if 'linear_feature' in key:
            value = np.transpose(value, (1, 0))
        else:
            value = np.transpose(value, (2, 3, 1, 0))
    try:
        sess.run(tf.assign(var, value))
    except:
        print(var, h5f[key][()].shape)


