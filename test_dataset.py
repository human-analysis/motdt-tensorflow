import numpy as np
import os

# from mock import self
from scipy.misc import imread
# import random
import tensorflow as tf
from libs.utils.io import read_mot_results, unzip_objs, config_parser
# from libs.utils.common import shuffle_list


class MOTSeq:
    def __init__(self, root, det_root, seq_name, min_height, min_det_score, sess, batch_size=1):
        self.root = root
        self.seq_name = seq_name
        self.min_height = min_height
        self.min_det_score = min_det_score

        self.im_root = os.path.join(self.root, self.seq_name, 'img1')
        self.info_file = os.path.join(self.root, self.seq_name, 'seqinfo.ini')
        self.seq_info = config_parser(self.info_file)
        print(self.seq_info)
        self.im_names = sorted([name for name in os.listdir(self.im_root) if os.path.splitext(name)[-1] == '.jpg'])

        if det_root is None:
            self.det_file = os.path.join(self.root, self.seq_name, 'det', 'det.txt')
        else:
            self.det_file = os.path.join(det_root, '{}.txt'.format(self.seq_name))
        self.dets = read_mot_results(self.det_file, is_gt=False, is_ignore=False)

        self.gt_file = os.path.join(self.root, self.seq_name, 'gt', 'gt.txt')
        if os.path.isfile(self.gt_file):
            self.gts = read_mot_results(self.gt_file, is_gt=True, is_ignore=False)
        else:
            self.gts = None
        self.batch_size = batch_size
        self.sess = sess
        self.dataset_controller()

    def __len__(self):
        return len(self.im_names)

    def get_item(self, i):
        im_name = os.path.join(self.im_root, self.im_names[i])
        # im = cv2.imread(im_name)
        im = imread(im_name)  # rgb
        im = im[:, :, ::-1]  # bgr

        frame = i + 1
        dets = self.dets.get(frame, [])
        tlwhs, _, scores = unzip_objs(dets)
        scores = np.asarray(scores)

        keep = (tlwhs[:, 3] >= self.min_height) & (scores > self.min_det_score)
        tlwhs = tlwhs[keep]
        scores = scores[keep]

        if self.gts is not None:
            gts = self.gts.get(frame, [])
            gt_tlwhs, gt_ids, _ = unzip_objs(gts)
        else:
            gt_tlwhs, gt_ids = None, None

        return im, tlwhs, scores, gt_tlwhs, list(gt_ids)

    def data_generator(self):
        id = -1
        while True:
            # if i % len(self.im_names) == 0:
            #     if self.gts is not None:
            #         self.im_names, self.dets, self.gts = \
            #             shuffle_list(self.im_names, self.dets, self.gts)
            #     else:
            #         self.im_names, self.dets = \
            #             shuffle_list(self.im_names, self.dets)
            #     i = 0
            id = (id + 1) % len(self.im_names)
            yield self.get_item(id)

    def dataset_controller(self):
        output_shapes = (tf.TensorShape([int(self.seq_info['imHeight']), int(self.seq_info['imWidth']), 3]),
                         tf.TensorShape([None, 4]), tf.TensorShape([None]), tf.TensorShape([None, 4]), tf.TensorShape([None]))
        self.max_value = tf.placeholder(tf.int64, shape=[])
        dataset = tf.data.Dataset.from_generator(generator=self.data_generator, output_shapes=output_shapes,
                        output_types=(tf.float32, tf.float32, tf.float32, tf.float32, tf.int64))
        self.dataset = dataset.prefetch(buffer_size=100)
        # self.dataset = dataset.range(self.max_value)
        self.iterator = self.dataset.make_one_shot_iterator()
        self.next_element = self.iterator.get_next()
        print('Epoch dataset, {}'.format(dataset))

    # def take(self, se):
    #     return self.dataset.take(1)
    def next(self):
        return self.sess.run(self.next_element)

    def __iter__(self):
        i = 0
        while i < len(self.im_names):
            yield self.next()
            i += 1


def get_loader(root, det_root, name, sess, min_height=0, min_det_score=-np.inf):
    dataset = MOTSeq(root, det_root, name, min_height, min_det_score, sess=sess)

    return dataset

if __name__ == '__main__':
    sess = tf.Session()
    dataset = get_loader(root='storage/MOT16/train', det_root=None,
         name='MOT16-02', sess=sess)
    # x = next(dataset)
    # sess = tf.Session()
    # print(sess.run(dataset.next_element))
    # sess.run(dataset.iterator.intializer, feed_dict={dataset.max_value: 10})
    # for i in range(10):
    #     value = sess.run(dataset.next_element)
    #     print(value)
    #     assert len(value) == 3
    # print(next(dataset.data_generator()))
    # print(dataset.get_next(sess))
    # print(len(dataset))
    for i, data in enumerate(dataset):
        if i == 10:
            assert 1 == 0
        print(data[0].shape)
    # assert 1 == 0
