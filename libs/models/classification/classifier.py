import numpy as np
import cv2
import tensorflow as tf
from libs.models.classification.rfcn_cls import Model as CLSModel
from libs.utils.common import check
from libs.utils.net_utils import load_pretrained
from libs.utils.bbox_utils import clip_boxes
from libs.utils.image_processing import crop_with_factor


class PatchClassifier(object):
    def __init__(self, scope: object, sess: object) -> object:
        self.scope = scope
        self.sess = sess
        # with self.g.as_default() as g:
        self.model = CLSModel(extractor='squeezenet')
        self.build_model()
        self.img_scale = 1.

    def build_model(self):
        with tf.name_scope(self.scope):
            with tf.variable_scope(self.scope):
                self.input = tf.placeholder(dtype=tf.float32, shape=[None, 640, 1152, 3])
                self.inp_size = tf.shape(self.input)
                self.feature_map = self.model(self.input)
                # print('feature_map', self.feature_map)
            self.get_pretrained_variables()
        # self.get_pretrained_variables()
        self.load_pretrain()

    def get_pretrained_variables(self):
        self.vars = []
        for var in tf.global_variables(scope=self.scope):
            self.vars.append(var)

    def load_pretrain(self, ckpt='storage/nets/squeezenet_small40_coco_mot16_ckpt_10.h5'):
        load_pretrained(self.sess, self.vars, ckpt)

    @staticmethod
    def im_preprocess(image):
        # resize and padding
        # real_inp_size = min_size
        if min(image.shape[0:2]) > 720:
            real_inp_size = 640
        else:
            real_inp_size = 368
        im_pad, im_scale, real_shape = crop_with_factor(image, real_inp_size, factor=16, pad_val=0, basedon='min')

        # preprocess image
        im_croped = cv2.cvtColor(im_pad, cv2.COLOR_BGR2RGB)
        im_croped = im_croped.astype(np.float32) / 255. - 0.5

        return im_croped, im_pad, real_shape, im_scale

    def update(self, image):
        croped_img, im_pad, real_shape, im_scale = self.im_preprocess(image)
        self.im_scale = im_scale
        self.ori_image_shape = image.shape
        im_data = np.expand_dims(croped_img, 0)
        # im_data = croped_img
        # print(self.input, im_data.shape)
        self.im_data = im_data

        self.scope_map = self.sess.run(self.feature_map, {self.input: im_data})

        return real_shape, im_scale

    def predict(self, rois):
        """
        :param img: 
        :param sess: 
        :param rois: given numpy array with size (N, 4) (x1, y1, x2, y2)
        :return: scores = [N]
        """
        # 5 .build rfcn head
        # print(len(rois), 'test bugs')
        if len(rois) == 0:
            return []
        with tf.name_scope(self.scope):
            scaled_rois = rois * self.im_scale
            # print(scaled_rois, 'scaled_rois')
            self.cls_scores = self.model.get_cls_score(self.scope_map, scaled_rois, self.inp_size)
            # print(self.sess.run(cls_scores, feed_dict={self.img: np.expand_dims(img, 0)}))
            # check area
        cls_scores_value = self.sess.run(self.cls_scores, feed_dict={self.input: self.im_data})
        # print(cls_scores_value, 'cls_scores_value')

        if len(cls_scores_value.shape) == 0:
            cls_scores_value = np.expand_dims(cls_scores_value, 0)

        # cls_scores_value = np.array(cls_scores_value)

        # cls_scores_value = cls_scores_value.reshape((20,))
        rois = rois.reshape(-1, 4)
        clipped_boxes = clip_boxes(rois, self.ori_image_shape)

        ori_areas = (rois[:, 2] - rois[:, 0]) * (rois[:, 3] - rois[:, 1])
        areas = (clipped_boxes[:, 2] - clipped_boxes[:, 0]) * (clipped_boxes[:, 3] - clipped_boxes[:, 1])
        ratios = areas / np.clip(ori_areas, a_min=1e-4, a_max=None)
        assert type(ratios) == np.ndarray
        # print(ratios.shape, cls_scores_value.shape)
        cls_scores_value[ratios < 0.5] = 0

        return cls_scores_value
