import tensorflow as tf
import tensorflow.contrib.slim as slim
from libs.models.backbone import SqueezeNetx16
from libs.configs import cfgs
from libs.lib_kernel import psroi_warp


class Model(object):

    def __init__(self, extractor='squeezenet', is_training=True):
        self.extractor_name = extractor
        self.is_training = is_training
        self.psroipool_cls = None
        self.avg_pool = None

    def build_feature_extractor(self, imgs):
        if self.extractor_name.startswith('squeezenet'):
            return SqueezeNetx16.squeeze_base(imgs, is_training=self.is_training, pretrained=True)
        else:
            raise ValueError('Sorry, we only support resnet or mobilenet_v2')

    def build_head_model(self, slice_rpn_base):
        if self.extractor_name.startswith('squeezenet'):
            return SqueezeNetx16.squeeze_head(slice_rpn_base, is_training=self.is_training)
        else:
            raise ValueError('Sorry, we only support resnet or mobilenet_v2')

    def build_upconv_layer(self, input, out_channels, index):
        with tf.variable_scope('Upsample_{}'.format(index)):
            x = slim.conv2d(input, out_channels, [3, 3], trainable=self.is_training,
                            weights_initializer=cfgs.INITIALIZER, activation_fn=None,
                            scope='upconv_{}.0'.format(index), biases_initializer=tf.constant_initializer(0.0))
            x = tf.layers.batch_normalization(x, name='upconv_{}.1'.format(index))
            x = tf.nn.relu(x)
            width, height = x.get_shape().as_list()[1:3]
            x = tf.image.resize_bilinear(x, (width * 2, height * 2))
            return x

    def build_dialation_layer(self, input, out_channels, kernel_size, scope, bn=False):
        x = slim.conv2d(input, out_channels, [kernel_size, kernel_size], scope=scope)
        if bn:
            x = tf.layers.batch_normalization(x, scope='Dconv_bn')
        return x

    def build_proj_layer(self, input, out_channels, index):
        with tf.variable_scope('Proj_{}'.format(index)):
            x = self.build_dialation_layer(input, out_channels // 2, 3, scope='proj_{}.0.0.Dconv'.format(index))
            x = self.build_dialation_layer(input, out_channels // 2, 5, scope='proj_{}.0.1.Dconv'.format(index)) + x
            x = slim.conv2d(x, out_channels // 2, [1, 1], scope='proj_{}.1'.format(index), activation_fn=None)
            x = tf.layers.batch_normalization(x, name='proj_{}.2'.format(index))
            x = tf.nn.relu(x)
            # in_channels = out_channels + out_channels // 2
            return x

    @staticmethod
    def psroi_pooling(feature_maps, rois, crop_size=[7, 7], num_spatial_bins=[3, 3]):
        """
        # like PSROIPoolFunction
        :param featuremaps_dict: feature map to crop
        :param rois: shape is [-1, 4]. [x1, y1, x2, y2]
        :return:
        """

        with tf.variable_scope('PSROI_Pooling'):
            result = psroi_warp.position_sensitive_crop_regions(image=feature_maps,
                                                                boxes=rois,
                                                                crop_size=crop_size,
                                                                num_spatial_bins=num_spatial_bins,
                                                                global_pool=True)

            return result

    def build_rfcn_head(self, rfcn_cls, rois, img_shape, bin_nums=[7, 7], crop_size=[49, 49]):
        with tf.variable_scope('RFCN'):
            # 5. PSROI Pooling
            rois = tf.convert_to_tensor(rois, dtype=tf.float32)
            # deal the boxes
            img_h, img_w = tf.cast(img_shape[1], tf.float32), tf.cast(img_shape[2], tf.float32)

            # print(rois, 1111111111111111111111111111)

            x1, y1, x2, y2 = tf.unstack(rois, axis=1)

            normalized_x1 = x1 / img_w
            normalized_x2 = x2 / img_w
            normalized_y1 = y1 / img_h
            normalized_y2 = y2 / img_h

            normalized_rois = tf.transpose(
                tf.stack([normalized_y1, normalized_x1, normalized_y2, normalized_x2]), name='get_normalized_rois')

            normalized_rois = tf.stop_gradient(normalized_rois)

            cls_score = self.psroi_pooling(feature_maps=rfcn_cls, rois=normalized_rois, crop_size=crop_size,
                                           num_spatial_bins=bin_nums)

            # rfcn_bbox_list = tf.split(rfcn_bbox, num_or_size_splits=(cfgs.CLASS_NUM + 1), axis=3)

            # bbox_pred_all_class = []
            # for bbox in rfcn_bbox_list:
            #     bbox_pred = self.psroi_pooling(feature_maps=bbox, rois=normalized_rois, crop_size=crop_size,
            #                                    num_spatial_bins=bin_nums)
            #
            #     bbox_pred = tf.reshape(bbox_pred, [-1, 4])
            #     bbox_pred_all_class.append(bbox_pred)
            # bbox_pred = tf.concat(bbox_pred_all_class, axis=1)
            # cls_score = tf.reshape(cls_score, [-1, cfgs.CLASS_NUM + 1])
            # bbox_pred = tf.reshape(bbox_pred, [1, (cfgs.CLASS_NUM + 1)])

            # psroipooled_cls_rois, psroipooled_loc_rois = self.psroi_pooling(rfcn_cls, rfcn_bbox, rois, group_size=7,
            #                                                                 spatial_scale=1.0/cfgs.ANCHOR_STRIDE[0])
            # cls_score = tf.reduce_mean(psroipooled_cls_rois, axis=[1, 2])
            # bbox_pred = tf.reduce_mean(psroipooled_loc_rois, axis=[1, 2])
            # bbox_pred = tf.tile(bbox_pred, [1, (cfgs.CLASS_NUM + 1)])

            # cls_score = tf.reshape(cls_score, [-1, cfgs.CLASS_NUM+1])
            # bbox_pred = tf.reshape(bbox_pred, [-1, (cfgs.CLASS_NUM + 1) * 4])
            # cls_score = tf.reshape(cls_score, [-1, 2])
            cls_score = tf.squeeze(cls_score)
            # cls_score = tf.reduce_max(cls_score, axis=-1)
            # print(cls_score, 'cls_score')

        return cls_score

    def get_cls_score(self, cls_feat, rois, img_shape):
        cls_score = self.build_rfcn_head(cls_feat, rois, img_shape)
        return cls_score

    def get_cls_score_numpy(self, sess, cls_feat, rois, img_shape):
        rois_ = tf.convert_to_tensor(rois, dtype=tf.float32)
        cls_score = self.build_rfcn_head(cls_feat, rois_, img_shape, bin_nums=[3, 3], crop_size=[7, 7])
        # cls_score_numpy = sess.run(cls_score, )

    def __call__(self, imgs, gts=True):
        # 1.0 build the base network
        rpn_base = self.build_feature_extractor(imgs)
        # print('feats', rpn_base)
        # 1.1 build the head_base model
        rfcn_base = self.build_head_model(rpn_base[-1])
        # print('x_in', rfcn_base)
        # 2 build rpn head(up conv)
        n_feats = SqueezeNetx16.n_feats[1:]
        # print(rfcn_base)

        in_channels = 256
        out_cs = [128, 256]
        with tf.variable_scope('build_rpn'):
            for i in range(1, len(n_feats)):
                out_channels = out_cs[-i]
                x_depth_out = self.build_upconv_layer(rfcn_base, out_channels, i)
                x_project = self.build_proj_layer(rpn_base[-1-i], out_channels, i)
                rfcn_base = tf.concat([x_depth_out, x_project], -1)
                # print('x_in_{}'.format(i), rfcn_base)

                # in_channels = out_channels + out_channels // 2
            # cls features
            rfcn_cls_in = rfcn_base
            roi_size = 7
            filters = rfcn_cls_in.get_shape().as_list()[-1]
            cls_feat = slim.conv2d(rfcn_cls_in, filters, [3, 3], scope='cls_conv.0', activation_fn=None)
            cls_feat = tf.layers.batch_normalization(cls_feat, name='cls_conv.1')
            cls_feat = tf.nn.relu(cls_feat)
            cls_feat = tf.pad(cls_feat, [[0,0], [1, 1], [1, 1], [0, 0]])
            cls_feat = slim.conv2d(cls_feat, roi_size * roi_size, [1, 1], scope='cls_conv.3', activation_fn=None)
            # print('cls_feat', cls_feat)
            return cls_feat