import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np


def Inception(inputs, n1x1, n3x3red, n3x3, n5x5red, n5x5, pool_planes, scope=None):
    with tf.variable_scope(scope, [inputs]):
        with slim.arg_scope([slim.conv2d],
                            stride=1, padding='SAME', activation_fn=tf.nn.relu):
            # 1x1 branch
            branch_0 = slim.conv2d(inputs, n1x1, [1, 1], scope='b1/0')

            # 1x1 conv -> 3x3 conv branch
            branch_1 = slim.conv2d(inputs, n3x3red, [1, 1], scope='b2/0')
            branch_1 = slim.conv2d(branch_1, n3x3, [3, 3], scope='b2/2')

            # 1x1 conv -> 5x5 conv branch
            branch_2 = slim.conv2d(inputs, n5x5red, [1, 1], scope='b3/0')
            branch_2 = slim.conv2d(branch_2, n5x5, [5, 5], scope='b3/2')

            # 3x3 pool -> 1x1 conv branch
            branch_3 = slim.max_pool2d(inputs, [1, 1], padding='SAME', scope='AvgPool_0a_3x3')
            branch_3 = slim.conv2d(branch_3, pool_planes, [1, 1], scope='b4/1')

            return tf.concat([branch_0, branch_1, branch_2, branch_3], axis=-1)

def GoogLeNet(inputs):
    output_channels = 832
    with tf.variable_scope('feat_conv'):
        with tf.variable_scope('pre_layers'):
            with slim.arg_scope([slim.conv2d, slim.max_pool2d],
                        stride=1, padding='SAME'):
                net = slim.conv2d(inputs, 64, [7, 7], stride=2, scope='0')

                net = slim.max_pool2d(net, [3, 3])
                net = tf.nn.local_response_normalization(net)

                net = slim.conv2d(net, 64, [1, 1], scope='4')
                net = slim.conv2d(net, 192, [3, 3], scope='6')

                net = tf.nn.local_response_normalization(net)
                net = slim.max_pool2d(net, [3, 3])

        with slim.arg_scope([slim.conv2d, slim.max_pool2d],
            stride=1, padding='SAME'):
            net = Inception(net, 64, 96, 128, 16, 32, 32, scope='a3')
            net = Inception(net, 128, 128, 192, 32, 96, 64, scope='b3')
            net = slim.max_pool2d(net, [3, 3])
            net = Inception(net, 192, 96, 208, 16, 48, 64, scope='a4')
            net = Inception(net, 160, 112, 224, 24, 64, 64, scope='b4')
            net = Inception(net, 128, 128, 256, 24, 64, 64, scope='c4')
            net = Inception(net, 112, 144, 288, 32, 64, 64, scope='d4')
            net = Inception(net, 256, 160, 320, 32, 128, 128, scope='e4')
            return net

# img = tf.placeholder(tf.float32, [4, 64, 64, 3])
# y = GoogLeNet(img)