from random import shuffle


def check(name):
    if "weights:0" in name or "biases:0" in name or "gamma:0" in name or "beta:0" in name \
            or "moving_variance:0" in name or "moving_mean:0" in name:
        return True
    return False


def shuffle_list(*ls):
    l = list(zip(*ls))

    shuffle(l)
    return zip(*l)

if __name__ == '__main__':
    a = [0, 1, 2, 3, 4]
    b = [5, 6, 7, 8, 9]
    a1, b1 = shuffle_list(a, b)
    print(a1, b1)