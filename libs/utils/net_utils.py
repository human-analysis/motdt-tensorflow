import h5py
import tensorflow as tf
import numpy as np


def load_pretrained(sess, vars, h5_file):
    # for var in vars:
    #     print(var)
    # assert 1 == 0
    h5f = h5py.File(h5_file)
    param_counter = 0
    for var in vars:
        name = var.name
        if 'feature_extractor' in name:
            param = '.'.join(name.split('/')[1:]).replace('biases:0', 'bias').replace('weights:0', 'weight')

            value = h5f[param][()]
            if len(var.get_shape().as_list()) == 4:
                value = np.transpose(value, (2, 3, 1, 0))
            sess.run(tf.assign(var, value))
            param_counter += 1
        elif 'stage_0.1' in name:
            param = '.'.join(name.split('/')[1:]).replace('biases:0', 'bias').replace('weights:0', 'weight')
            value = h5f[param][()]

            if len(var.get_shape().as_list()) == 4:
                value = np.transpose(value, (2, 3, 1, 0))
            sess.run(tf.assign(var, value))
            param_counter += 1
        elif 'cls_conv' in name:
            if 'cls_conv.1' in name:
                type_of_param = name.split('/')[-1]
                param = '.'.join(name.split('/')[-2:])
                if type_of_param == 'gamma:0':
                    param = param.replace(type_of_param, 'weight')
                elif type_of_param == 'beta:0':
                    param = param.replace(type_of_param, 'bias')
                elif type_of_param == 'moving_mean:0':
                    param = param.replace(type_of_param, 'running_mean')
                elif type_of_param == 'moving_variance:0':
                    param = param.replace(type_of_param, 'running_var')
                else:
                    ValueError('Tensorflow model only support for the above parameter!!')
                sess.run(tf.assign(var, h5f[param][()]))
            else:
                param = '.'.join(name.split('/')[-2:]).replace('biases:0', 'bias').replace('weights:0', 'weight')
                # print(var, h5f[param][()].shape)
                value = h5f[param][()]
                if len(var.get_shape().as_list()) == 4:
                    value = np.transpose(value, (2, 3, 1, 0))
                sess.run(tf.assign(var, value))
            param_counter += 1

        elif 'Upsample' in name:
            if 'upconv_1.1' in name or 'upconv_2.1' in name:
                type_of_param = name.split('/')[-1]
                param = '.'.join(name.split('/')[-2:])
                # print(type_of_param)
                if type_of_param == 'gamma:0':
                    # print(param)
                    param = param.replace('gamma:0', 'weight')
                    # print(param)
                elif type_of_param == 'beta:0':
                    param = param.replace('beta:0', 'bias')
                elif type_of_param == 'moving_mean:0':
                    param = param.replace('moving_mean:0', 'running_mean')
                elif type_of_param == 'moving_variance:0':
                    param = param.replace('moving_variance:0', 'running_var')
                else:
                    ValueError('Tensorflow model only support for the above parameter!!')
                # print(param)
                sess.run(tf.assign(var, h5f[param][()]))
            else:
                param = '.'.join(name.split('/')[-2:]).replace('biases:0', 'bias').replace('weights:0', 'weight')
                value = h5f[param][()]
                if len(var.get_shape().as_list()) == 4:
                    value = np.transpose(value, (2, 3, 1, 0))
                sess.run(tf.assign(var, value))
            param_counter += 1

        else:
            if 'proj_1.2' in name or 'proj_2.2' in name:
                type_of_param = name.split('/')[-1]
                param = '.'.join(name.split('/')[-2:])
                if type_of_param == 'gamma:0':
                    param = param.replace(type_of_param, 'weight')
                elif type_of_param == 'beta:0':
                    param = param.replace(type_of_param, 'bias')
                elif type_of_param == 'moving_mean:0':
                    param = param.replace(type_of_param, 'running_mean')
                elif type_of_param == 'moving_variance:0':
                    param = param.replace(type_of_param, 'running_var')
                else:
                    ValueError('Tensorflow model only support for the above parameter!!')
                sess.run(tf.assign(var, h5f[param][()]))
            else:
                param = '.'.join(name.split('/')[-2:]).replace('biases:0', 'bias').replace('weights:0', 'weight')
                value = h5f[param][()]
                if len(var.get_shape().as_list()) == 4:
                    value = np.transpose(value, (2, 3, 1, 0))

                sess.run(tf.assign(var, value))

            param_counter += 1
    assert param_counter == len(h5f.keys())

def load_pretrained_(sess, vars, h5_file):
    # ckpt = 'storage/nets/googlenet_part8_all_xavier_ckpt_56.h5'
    h5f = h5py.File(h5_file)
    assert len(h5f.keys()) == len(vars)
    for var in vars:
        key = '.'.join(var.name.split('/')[1:]).replace('weights:0', 'weight').replace('biases:0', 'bias')
        # print(key)
        value = h5f[key][()]
        if 'weight' in key:
            if 'linear_feature' in key:
                value = np.transpose(value, (1, 0))
            else:
                value = np.transpose(value, (2, 3, 1, 0))
        try:
            sess.run(tf.assign(var, value))
        except:
            print(var, h5f[key][()].shape)

